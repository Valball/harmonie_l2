package v2;

import java.util.*;

/**
 * Classe Harmonie, contient l'harmonie calculé pour le chant donné
 * 
 * @author Valentin
 * 
 */
public class Harmonie {
	private ArrayList<Temps> list;

	/**
	 * Retourne la liste de temps de l'harmonie
	 */
	public ArrayList<Temps> getList() {
		return list;
	}

	/**
	 * Rajoute un Temps à la liste
	 */
	public void add(Temps t) {
		list.add(t);
	}

	/**
	 * Constructeur d'une Harmonie vierge (vide)
	 */
	public Harmonie() {
		list = new ArrayList<Temps>();
	}

	/**
	 * Retourne le contenue de la liste sous forme de String
	 */
	public String toString() {
		Iterator<Temps> it = list.iterator();
		Temps t;
		String s = "";
		while (it.hasNext()) {
			t = it.next();
			s += t.toString() + "\n";
		}
		return s;
	}

}