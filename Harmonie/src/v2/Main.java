package v2;

import java.io.*;
import java.util.*;

import javax.sound.midi.*;

import exceptions.*;

/**
 * Classe executable
 */
public class Main {
	/**
	 * Tableau des accord
	 */
	static Accord[] acc = new Accord[8];
	/**
	 * Titre du chant en cours
	 */
	static String titre;
	/**
	 * Le chant tel qu'il était au départ (avec les silences et les temps)
	 */
	static ArrayList<Integer> chantdépart;

	/**
	 * Programme principal
	 */
	public static void main(String[] args) {
		for (int i = 0; i < acc.length; i++) {
			acc[i] = new Accord(i + 1);
		} // création des accord
		try {
			switch (args[0]) { // choix de l'option
			case "-name":
				System.out
						.println("Jubert Valentin\nNigon Quentin\nHuang Jungang\nEl Maftah Mohamed");
				break;
			case "-ly":
			case "-midi":
				verifarglong3(args);
				dérouleur(new File(args[1]), 4, args[0], args[2]);
				// le chant, la règle de beauté général, l'option ly ou midi, le
				// fichier à écrire
				break;
			case "-h":
				affichageoption();
				break;
			case "-beaute":
				int beaute = Integer.parseInt(args[1]);
				if (!verifbeaute(beaute)) {
					throw new BeauteIncorrect();
				} else {
					dérouleur(new File(args[3]), beaute, args[2], args[4]);
					// le chant, le numéro de la règle de beaute, l'option ly ou
					// midi, le fichier à écrire
				}
				break;
			case "-w":
				verifarglong3(args);
				verifdossier(args[1], args[2]);
				dérouleurdossier(new File(args[1]), new File(args[2]));
				// les deux dossiers
				break;
			case "-nombre":
				verifarg1(args);
				System.out.println("Le nombre d'harmonisation possible est "
						+ dérouleurnombre(new File(args[1])));
				break;
			default:
				System.out
						.println("Option non reconnue\nEssayez l'option -h pour avoir l'aide");
				System.exit(3);
			}
		} catch (ParamNumberIncorrectException e) {
			// Nombre de paramètre incorrect par rapport à l'option désiré
			System.out.println("Nombre de paramètre incorrect");
			System.exit(1);
		} catch (ParamFormatException e) {
			// Format des paramètres incorrect
			System.out.println("Format du paramètre " + e.getNumparam()
					+ " incorrect");
			System.exit(2);
		} catch (BeauteIncorrect e) {
			// le numéro de la règle de beauté n'existe pas
			System.out.println(args[1] + " n'est pas une règle de beauté");
			System.exit(5);
		} catch (NumberFormatException e) {
			// Lorsqu'on devrait avoir un entier, on n'en a pas un
			System.out.println("Erreur dans les paramètres");
			System.exit(4);
		} catch (VoidFileException e) {
			System.out
					.println("Le fichier .chant passé en argument était vide");
			System.exit(6);
		} catch (EnsTempsIncVide e) {
			System.out.println("Erreur dans le chant de départ");
			System.exit(7);
		}
	}

	// vérifie si les deux string pointent bien des dossiers
	private static void verifdossier(String d1, String d2)
			throws ParamFormatException {
		if (!new File(d1).isDirectory())
			throw new ParamFormatException(2);
		File f2 = new File(d2);
		if (f2.exists() && !f2.isDirectory())
			throw new ParamFormatException(3);
	}

	// vérifie si la chaine qui pointent vers le fichier chant est bien un chant
	private static void verifarg1(String[] args) throws ParamFormatException {
		if (!args[1].endsWith(".chant")) {
			throw (new ParamFormatException(1));
		}
	}

	// vérifie le nombre d'argument
	private static void verifarglong3(String[] args)
			throws ParamNumberIncorrectException, ParamFormatException {
		if (args.length != 3)
			throw (new ParamNumberIncorrectException());
	}

	/**
	 * Instruction à faire pour générer l'harmonie et le fichier désiré
	 * 
	 * @param àlire
	 *            Fichier chant à lire
	 * @param beaute
	 *            Numéro de la règle de beauté
	 * @param option
	 *            ly ou midi, pour l'écriture du fichier
	 * @param àécrire
	 *            chaine du fichier à écrire
	 * @throws VoidFileException
	 *             Si le fichier chant est vide
	 * @throws EnsTempsIncVide
	 *             S'il y a une erreur dans le chant de départ
	 */
	public static void dérouleur(File àlire, int beaute, String option,
			String àécrire) throws VoidFileException, EnsTempsIncVide {
		int[] part = lectureChant(àlire);
		// on obtient les soprano de départ
		ArrayList<TempsIncomplet> list = suite(part);
		// on obtient tous les Temps possibles à tous les instants
		apprègle5et6(list);
		// on génére les suivants
		appsupp(list);
		// on supprime les temps qui ne donnent pas de suivant
		ArrayList<Temps> premiers = list.get(0).getLocale();
		Temps départ = new Temps(premiers);
		// on forme le puit (théorie des graphes)
		Harmonie m = parcours(départ, beaute);
		// on obtient la meilleure harmonie possible
		if (m.getList().size() != part.length) {
			/*
			 * il y a un bug qu'on arrive pas à résoudre des fois il tombe sur
			 * des suivants vide
			 */
			System.out
					.println("Désolé, le programme a rencontré une erreur inconnue");
		}
		switch (option) {// l'écriture du fichier
		case "-ly":
			écriturely(m, new File(àécrire));
			break;
		case "-midi":
			écrituremid(m, new File(àécrire));
			break;
		}
	}

	/**
	 * Applique la fonction supprsuivantvide()
	 * 
	 * @see TempsIncomplet
	 * @param list
	 *            La liste des Temps sur lesquels appliquer la fonction
	 */
	public static void appsupp(ArrayList<TempsIncomplet> list) {
		for (int i = list.size() - 2; i >= 0; i--) {
			// on s'arrête quelque temps avant la fin car évidemment les
			// derniers temps n'ont pas de suivant
			list.get(i).supprsuivantvide();
		}
	}

	/**
	 * Instruction pour obtenir le nombre d'harmonisation possible
	 * 
	 * @param àlire
	 *            Fichier chant à lire
	 * @return Le nombre d'harmonisation possible
	 * @throws VoidFileException
	 *             Si le Fichier chant est vide
	 * @throws EnsTempsIncVide
	 *             S'il y a un erreur dans le fichier de départ
	 */
	public static int dérouleurnombre(File àlire) throws VoidFileException,
			EnsTempsIncVide {
		int[] part = lectureChant(àlire);
		ArrayList<TempsIncomplet> list = suite(part);
		apprègle5et6(list);
		appsupp(list);
		ArrayList<Temps> premiers = list.get(0).getLocale();
		Temps départ = new Temps(premiers);
		return nombre(départ);
	}

	// vérifie si le numéro passé en argument indique une règle de beauté
	private static boolean verifbeaute(int i) {
		return (i >= 1 && i <= 4);
	}

	/**
	 * Lit le fichier chant et retourne les soprano
	 * 
	 * @param àlire
	 *            Le fichier chant à lire
	 * @return Le tableau des soprano à partir desquels travaillé
	 * @throws VoidFileException
	 *             Si le fichier est vide au départ
	 */
	public static int[] lectureChant(File àlire) throws VoidFileException {
		ArrayList<Integer> list = new ArrayList<Integer>();
		chantdépart = new ArrayList<Integer>();
		int[] part = null;
		try {
			Scanner lire = new Scanner(àlire);
			String a;
			int b, temps;
			if (lire.hasNextLine()) {
				// la première ligne est le titre du chant
				titre = lire.nextLine();
				if (!lire.hasNextLine()) {
					// s'il y a que le titre dans le fichier
					lire.close();
					throw new VoidFileException();
				}
			} else {
				// s'il est completement vide
				lire.close();
				throw new VoidFileException();
			}
			while (lire.hasNext()) {
				a = lire.next();
				b = encode(a);
				// on encode le soprano
				chantdépart.add(b);
				if (a.startsWith("-")) {
					// si c'est un silence alors on prend le soprano d'avant
					b = list.get(list.size() - 1);
				}
				list.add(b);
				temps = Integer.parseInt(a.split(":")[1]) - 1;
				// calcule le temps indiqué sur le chant
				if (temps >= 1) {
					for (int j = temps; j > 0; j--) {
						list.add(b);
						// si le soprano dure plus d'un temps on copie ce
						// soprano sur tous les temps qu'il dure
					}
				}
			}
			// on transforme l'arraylist en un tableau
			part = new int[list.size()];
			Iterator<Integer> it = list.iterator();
			Integer c;
			int cpt = 0;
			while (it.hasNext()) {
				c = it.next();
				part[cpt] = (int) c;
				cpt++;
			}
			lire.close();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (EncodeException e) {
			e.printStackTrace();
		}
		return part;
	}

	/**
	 * écriture d'un fichier lilypond
	 * 
	 * @param m
	 *            L'harmonie à écrire
	 * @param f
	 *            Le fichier à écrire
	 */
	public static void écriturely(Harmonie m, File f) {
		assert (chantdépart.size() == m.getList().size());
		try {
			f.createNewFile(); // créer un nouveau fichier s'il n'existe pas
								// déjà
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			FileWriter fw = new FileWriter(f);
			fw.write("\\include \"italiano.ly\""); // pouvoir écrire les notes
													// en français
			fw.write("\r\n\\header{\r\ntitle = \"" + titre + "\"\r\n}");
			fw.write("\r\n\\new ChoirStaff\r\n<<");
			// traitement du soprano qui est celui donné à la base et pas celui
			// généré
			fw.write("\r\n\\new Staff {\r\n\t\\set Staff.instrumentName =#\"");
			fw.write("Soprano \"\r\n\t\\clef treble\r\n\t\\relative do''{");
			fw.write("\r\n\t\t");
			Iterator<Integer> it2 = chantdépart.iterator();
			int b;
			while (it2.hasNext()) {
				b = it2.next();
				fw.write(convnote(decodesop(b)) + convtemps(decodetemps(b))
						+ " ");
			}
			fw.write("\r\n\t}\r\n}\r\n");
			// fin du traitement du soprano
			for (int i = 2; i <= 4; i++) {
				fw.write("\r\n\\new Staff {\r\n\t\\set Staff.instrumentName =#\"");
				switch (i) {
				case 2:
					fw.write("Alto \"\r\n\t\\clef treble\r\n\t\\relative do'' {");
					break;
				case 3:
					fw.write("Tenor \"\r\n\t\\clef treble\r\n\t\\relative do' {");
					break;
				case 4:
					fw.write("Basse \"\r\n\t\\clef bass\r\n\t\\relative do {");
					break;
				}
				fw.write("\r\n\t\t");
				Iterator<Temps> it = m.getList().iterator();
				Temps a;
				while (it.hasNext()) {
					a = it.next();
					switch (i) {
					case 2:
						fw.write(convnote(a.getAlt()) + " ");
						break;
					case 3:
						fw.write(convnote(a.getTen()) + " ");
						break;
					case 4:
						fw.write(convnote(a.getBas()) + " ");
						break;
					}
				}
				fw.write("\r\n\t}\r\n}\r\n");
			}
			fw.write(">>");
			fw.write("\r\n\\version \"2.16.0\"");
			fw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	// convertie un entier en temps lilypond
	private static String convtemps(int n) {
		switch (n) {
		case 1:
			return "4";
		case 2:
			return "2";
		case 3:
			return "2.";
		case 4:
			return "1";
		default:
			return "";
		}
	}

	// convertie un entier en une string pour lily
	private static String convnote(int n) {
		if (n == 28) {
			// silence
			return "r";
		} else {
			switch (n % 7) {
			case 0:
				return "do";
			case 1:
				return "re";
			case 2:
				return "mi";
			case 3:
				return "fa";
			case 4:
				return "sol";
			case 5:
				return "la";
			default:
				return "si";
			}
		}
	}

	/**
	 * Encode une string du chant
	 * 
	 * @param s
	 *            La String à convertir
	 * @return Le codage du soprano et du temps indiqué par la string
	 * @throws EncodeException
	 *             Si la string est incorrect
	 */
	public static int encode(String s) throws EncodeException {
		switch (s.substring(0, 2)) {
		case "do":
			return ((-7 + 7 * Integer.parseInt("0" + s.charAt(2))) << 2)
					+ Integer.parseInt("0" + s.charAt(4));
		case "re":
			return ((-6 + 7 * Integer.parseInt("0" + s.charAt(2))) << 2)
					+ Integer.parseInt("0" + s.charAt(4));
		case "mi":
			return ((-5 + 7 * Integer.parseInt("0" + s.charAt(2))) << 2)
					+ Integer.parseInt("0" + s.charAt(4));
		case "fa":
			return ((-4 + 7 * Integer.parseInt("0" + s.charAt(2))) << 2)
					+ Integer.parseInt("0" + s.charAt(4));
		case "so":
			return ((-3 + 7 * Integer.parseInt("0" + s.charAt(3))) << 2)
					+ Integer.parseInt("0" + s.charAt(5));
		case "la":
			return ((-2 + 7 * Integer.parseInt("0" + s.charAt(2))) << 2)
					+ Integer.parseInt("0" + s.charAt(4));
		case "si":
			return ((-1 + 7 * Integer.parseInt("0" + s.charAt(2))) << 2)
					+ Integer.parseInt("0" + s.charAt(4));
		case "-:":
			return ((28 << 2) + Integer.parseInt("0" + s.charAt(2)));
		}
		throw new EncodeException();
	}

	/**
	 * Décode le temps encode par encode(String s)
	 */
	public static int decodetemps(int n) {
		return (n - (decodesop(n) << 2));
	}

	/**
	 * Décode le soprano encode par encode(String s)
	 */
	public static int decodesop(int n) {
		return n >> 2;
	}

	/**
	 * Affiche les options possibles du programme
	 */
	public static void affichageoption() {
		System.out
				.println("Les différentes options du programme sont :\n-name : affiche les noms et prénoms des développeurs\n-h : rappelle la liste des options du programme\n-midi fichier1.chant fichier2.mid : donne une harmonisation du chant contenu dans\n\t fichier1.chant sous la forme d’un fichier midi fichier2.mid\n-ly fichier1.chant fichier2.mid : donne une harmonisation du chant contenu dans \n\tfichier1.chant sous la forme d’un fichier LiLyPond fichier2.ly\n-nombre fichier1.chant : écrit le nombre d’harmonisations du chant contenu dans fichier1.chant\n-beaute k -midi fichier1.chant fichier2.mid : donne une plus belle harmonisation du chant contenu dans\n\t fichier1.chant suivant le critère de beauté numéro k (k=1..4) sous la forme d’un fichier midi fichier2.mid\n-beaute k -ly fichier1.chant fichier2.mid : donne une plus belle harmonisation du chant contenu dans \n\tfichier1.chant suivant le critère de beauté numéro k (k=1..4) sous la forme d’un fichier LiLyPond fichier2.ly\n-w dossier1 dossier2 : produit pour chaque chant dans le dossier1 une harmonisation au format .ly et .mid et les place\n\tdans le dossier2 ainsi qu'un fichier html regroupant toutes les informations de ces fichiers");
	}

	private static ArrayList<TempsIncomplet> suite(int[] part) {
		// suite du programme qu convertie les soprano en TempsIncomplet
		ArrayList<TempsIncomplet> list = new ArrayList<TempsIncomplet>();
		TempsIncomplet a;
		for (int i = 0; i < part.length; i++) {
			a = new TempsIncomplet(part[i]);
			if (i == 0 || i == part.length - 1)
				// si c'est le premier ou le dernier temps application de la
				// règle 4
				a.apprègle4();
			list.add(a);
		}
		return list;
	}

	private static void apprègle5et6(ArrayList<TempsIncomplet> ens)
			throws EnsTempsIncVide {
		// application de la règle 5 et 6 sur les temps
		TempsIncomplet a1, a2;
		Temps b1, b2;
		Iterator<TempsIncomplet> it1 = ens.iterator();
		if (!it1.hasNext()) {
			throw new EnsTempsIncVide();
		}
		Iterator<Temps> it2;
		Iterator<Temps> it3;
		a1 = it1.next();
		while (it1.hasNext()) {
			it2 = a1.getLocale().iterator();
			a2 = it1.next();
			while (it2.hasNext()) {
				b1 = it2.next();
				it3 = a2.getLocale().iterator();
				while (it3.hasNext()) {
					b2 = it3.next();
					if (b1.règle5(b2) && b1.regle6(b2))
						b1.add(b2);
				}
			}
			a1 = a2;
		}
	}

	/**
	 * Création de l'harmonie
	 * 
	 * @param t
	 *            Temps de départ
	 * @param beaute
	 *            Règle de beauté à appliquer
	 * @return L'Harmonie généré
	 */
	public static Harmonie parcours(Temps t, int beaute) {
		Harmonie m = new Harmonie();
		while (!(t.isLast())) {
			t.calculetordonne(beaute);
			switch (beaute) {
			case 1:
				t = t.getSuiv().get(0);
				break; // si c'est la première règle on prend le premier (plus
						// petit) des suivants
			default:
				t = t.getSuiv().get(t.getSuiv().size() - 1);
				break; // sinon on prend le dernier (plus grand) des suivants
			}
			m.add(t);
		}
		m.add(t); // ajout du dernier temps (ne rentre pas dans la boucle vu que
					// c'est dernier
		return m;
	}

	/**
	 * Calcule le nombre de suivant d'un temps
	 */
	public static int nombre(Temps t) {
		if (t.isLast())
			// si ya pas de suivant : 1
			return 1;
		if (t.getNombre() != -1)
			return t.getNombre();
		// si on a déjà calculé pour ce temps, inutile de recalculer !
		else {
			Iterator<Temps> it = t.getSuiv().iterator();
			Temps a;
			int cpt = 0;
			while (it.hasNext()) {
				a = it.next();
				a.setNombre(nombre(a));
				cpt += a.getNombre();
			}
			t.setNombre(cpt);
			return cpt;
		}
	}

	/**
	 * ecriture du fichier midi
	 * 
	 * @param m
	 *            L'harmonie à écrire
	 * @param f
	 *            Le fichier dans lequel écrire
	 */
	public static void écrituremid(Harmonie m, File f) {
		int[] corres = new int[] { 36, 38, 40, 41, 43, 45, 47, 48, 50, 52, 53,
				55, 57, 59, 60, 62, 64, 65, 67, 69, 71, 72, 74, 76, 77, 79, 81,
				83 };
		try {
			f.createNewFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
		int velocity = 64;
		try {
			Sequence sequence = new Sequence(Sequence.PPQ, 1, 4);
			{
				Track track = sequence.createTrack();
				track.add(instrument(73, 1)); // flute
				Iterator<Integer> it2 = chantdépart.iterator();
				int b;
				int cpt = 0;
				while (it2.hasNext()) {
					b = it2.next();
					int toadd = decodesop(b);
					if (toadd != 28) {
						track.add(noteOn(corres[toadd], cpt, 1, velocity));
						cpt += decodetemps(b);
						track.add(noteOff(corres[toadd], cpt, 1));
					} else {
						cpt += decodetemps(b);
					}
				}
			}// block de traitement du soprano de base
			for (int i = 2; i <= 4; i++) {
				Track track = sequence.createTrack();
				switch (i) {
				case 2:
					track.add(instrument(11, i));
					break; // vibraphone
				case 3:
					track.add(instrument(12, i));
					break; // mariba
				case 4:
					track.add(instrument(32, i));
					break; // bass
				}
				Iterator<Temps> it = m.getList().iterator();
				Temps a;
				int cpt = 0;
				while (it.hasNext()) {
					a = it.next();
					int toadd = toadd(a, i);
					track.add(noteOn(corres[toadd], cpt, i, velocity));
					cpt++;
					track.add(noteOff(corres[toadd], cpt, i));
				}
			}
			MidiSystem.write(sequence, 1, f);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// Suivant i on sait quoi prendre parmi l'alto le tenor ou la basse
	private static int toadd(Temps a, int i) {
		assert (i <= 4 && i >= 2);
		switch (i) {
		case 2:
			return a.getAlt();
		case 3:
			return a.getTen();
		case 4:
			return a.getBas();
		}
		return 21; // Impossible d'arriver ici, donc 21 !
	}

	private static MidiEvent noteOn(int nKey, long lTick, int canal,
			int velocity) throws InvalidMidiDataException {
		return createNoteEvent(ShortMessage.NOTE_ON, nKey, velocity, lTick,
				canal);
	}

	private static MidiEvent noteOff(int nKey, long lTick, int canal)
			throws InvalidMidiDataException {
		return createNoteEvent(ShortMessage.NOTE_OFF, nKey, 0, lTick, canal);
	}

	private static MidiEvent instrument(int instrument, int canal)
			throws InvalidMidiDataException {
		return createNoteEvent(ShortMessage.PROGRAM_CHANGE, instrument, 0, 0,
				canal);
	}

	private static MidiEvent createNoteEvent(int nCommand, int nKey,
			int nVelocity, long lTick, int canal)
			throws InvalidMidiDataException {
		ShortMessage message = new ShortMessage();
		message.setMessage(nCommand, canal, nKey, nVelocity);
		return new MidiEvent(message, lTick);
	}

	/**
	 * Instruction pour faire les calculs dans des dossiers
	 * 
	 * @param d1
	 *            Le dossier contenant les chants
	 * @param d2
	 *            Le dossier contenant les résultats
	 * @throws VoidFileException
	 *             Si le fichier chant est vide
	 * @throws EnsTempsIncVide
	 *             S'il y a un probleme dans le fichier chant
	 */
	public static void dérouleurdossier(File d1, File d2)
			throws VoidFileException, EnsTempsIncVide {
		// Class permettant de filtrer les fichiers d'un dossier
		class FilterChant implements FileFilter {
			public boolean accept(File arg0) {
				// le fichier doit contenir à la fin de son nom ".chant" pour
				// que ça retourne vrai
				return arg0.getPath().endsWith(".chant");
			}
		}
		try {
			if (!d2.exists())
				d2.mkdir();
			// si d2 n'existe pas on le créer
			File[] fich_chant = d1.listFiles(new FilterChant());
			// tous les fichiers chant du d1
			File mid;
			File ly;
			FileWriter f = new FileWriter(new File("resultat.html"));
			// le fichier html regroupant les résultats
			f.write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\"\r\n\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\r\n<html lang=\"fr\" xml:lang=\"fr\" xmlns=\"http://www.w3.org/1999/xhtml\">\r\n<head>\r\n<meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\" />\r\n<title>Résultat</title>\r\n<link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\" />\r\n</head>\r\n<body>");
			f.write("\r\n<table border=\"1\" style=\"empty-cells : hide;\"><tr><th>\"Titre\"</th><th>\"Nombre\"</th><th>\"Fichier lily\"</th><th>\"Fichier midi\"</th></tr>\r\n");
			for (int i = 0; i < fich_chant.length; i++) {
				String nom = fich_chant[i].getPath().split(".chant")[0]
						.substring(d1.getPath().length());
				// le nom des fichiers chant
				mid = new File(d2, nom + ".mid");
				ly = new File(d2, nom + ".ly");
				// déroulage normal (voir dérouleur())
				int[] part = lectureChant(fich_chant[i]);
				ArrayList<TempsIncomplet> list = suite(part);
				apprègle5et6(list);
				appsupp(list);
				ArrayList<Temps> premiers = list.get(0).getLocale();
				Temps départ = new Temps(premiers);
				Harmonie m = parcours(départ, 4);
				if (m.getList().size() != part.length) {
					System.out
							.println("Désolé, le programme a rencontré une erreur inconnue");
				}
				écriturely(m, ly);
				écrituremid(m, mid);
				f.write("<tr><td>" + titre + "</td><td>" + nombre(départ)
						+ "</td><td><a href=\"" + ly.getAbsolutePath()
						+ "\">Lien</a></td><td><a href=\""
						+ mid.getAbsolutePath() + "\">Lien</a></td></tr>\r\n");
				// écriture des informations dans le fichier html
			}
			f.write("</table>\r\n</body>\r\n</html>");
			f.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}