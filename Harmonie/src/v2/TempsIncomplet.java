package v2;

import java.util.*;

/**
 * Classe TempsIncomplet Ne contient que le soprano et tous les temps possibles
 * par rapport à ce soprano Sert au début du programme lorsque l'on ne sait pas
 * encore par quel temps commencé
 * 
 */

public class TempsIncomplet {
	private int sop;
	private ArrayList<Temps> locale;

	/**
	 * Retourne le soprano
	 */
	public int getSop() {
		return sop;
	}

	/**
	 * Retoune la liste de tous les temps possibles (via les règles
	 * d'enchainement locale)
	 */
	public ArrayList<Temps> getLocale() {
		return locale;
	}

	/**
	 * Constructeur TempsIncomplet
	 * 
	 * @param n
	 *            Le soprano codé
	 */
	public TempsIncomplet(int n) {
		sop = n >> 2;
		// décodage du soprano
		locale = new ArrayList<Temps>();
		generationlocale();
		// génération de tous les temps possibles
	}

	// fait la génération de tous les temps possibles
	private void generationlocale() {
		Accord[] acc = Main.acc;
		Temps a;
		for (int i = 0; i < acc.length; i++) {
			switch (acc[i].estDansAcc(sop)) {
			case 0:
				break; // le soprano n'est pas dans l'accord
			case 1: // le soprano est la tonique de l'accord
				for (int j = 0; j < 4; j++) {
					for (int k = 0; k < 4; k++) {
						for (int l = 0; l < 4; l++) {
							a = new Temps(sop, j * 7 + acc[i].getQui(), k * 7
									+ acc[i].getTie(), l * 7 + acc[i].getTon(),
									acc[i]);
							if (a.regle1() && a.regle2())
								locale.add(a);
							a = new Temps(sop, j * 7 + acc[i].getTie(), k * 7
									+ acc[i].getQui(), l * 7 + acc[i].getTon(),
									acc[i]);
							if (a.regle1() && a.regle2())
								locale.add(a);
						}
					}
				}
				break;
			case 2: // le soprano est la tierce de l'accord
				for (int j = 0; j < 4; j++) {
					for (int k = 0; k < 4; k++) {
						for (int l = 0; l < 4; l++) {
							a = new Temps(sop, j * 7 + acc[i].getQui(), k * 7
									+ acc[i].getTie(), l * 7 + acc[i].getTon(),
									acc[i]);
							if (a.regle1() && a.regle2())
								locale.add(a);
							a = new Temps(sop, j * 7 + acc[i].getTie(), k * 7
									+ acc[i].getQui(), l * 7 + acc[i].getTon(),
									acc[i]);
							if (a.regle1() && a.regle2())
								locale.add(a);
							a = new Temps(sop, j * 7 + acc[i].getQui(), k * 7
									+ acc[i].getTon(), l * 7 + acc[i].getTon(),
									acc[i]);
							if (a.regle1() && a.regle2())
								locale.add(a);
							a = new Temps(sop, j * 7 + acc[i].getQui(), k * 7
									+ acc[i].getQui(), l * 7 + acc[i].getTon(),
									acc[i]);
							if (a.regle1() && a.regle2())
								locale.add(a);
							a = new Temps(sop, j * 7 + acc[i].getTon(), k * 7
									+ acc[i].getQui(), l * 7 + acc[i].getTon(),
									acc[i]);
							if (a.regle1() && a.regle2())
								locale.add(a);
						}
					}
				}
				break;
			case 3: // le soprano est la quinte de l'accord
				for (int j = 0; j < 4; j++) {
					for (int k = 0; k < 4; k++) {
						for (int l = 0; l < 4; l++) {
							a = new Temps(sop, j * 7 + acc[i].getTie(), k * 7
									+ acc[i].getTon(), l * 7 + acc[i].getTon(),
									acc[i]);
							if (a.regle1() && a.regle2())
								locale.add(a);
							a = new Temps(sop, j * 7 + acc[i].getTie(), k * 7
									+ acc[i].getQui(), l * 7 + acc[i].getTon(),
									acc[i]);
							if (a.regle1() && a.regle2())
								locale.add(a);
							a = new Temps(sop, j * 7 + acc[i].getTie(), k * 7
									+ acc[i].getTie(), l * 7 + acc[i].getTon(),
									acc[i]);
							if (a.regle1() && a.regle2())
								locale.add(a);
							a = new Temps(sop, j * 7 + acc[i].getQui(), k * 7
									+ acc[i].getTie(), l * 7 + acc[i].getTon(),
									acc[i]);
							if (a.regle1() && a.regle2())
								locale.add(a);
							a = new Temps(sop, j * 7 + acc[i].getTon(), k * 7
									+ acc[i].getTie(), l * 7 + acc[i].getTon(),
									acc[i]);
							if (a.regle1() && a.regle2())
								locale.add(a);
						}
					}
				}
				break;
			}
		}
	}

	/**
	 * Applique la règle 4 : Le premier et le dernier Temps ne peuvent pas de
	 * l'accord 4b
	 */
	public void apprègle4() {
		Temps a;
		Iterator<Temps> it = locale.iterator();
		while (it.hasNext()) {
			a = it.next();
			if (a.règle4())
				it.remove();
		}
	}

	/**
	 * Affiche les temps locales
	 */
	public void affichagelocale() {
		Iterator<Temps> it = locale.iterator();
		Temps t;
		while (it.hasNext()) {
			t = it.next();
			System.out.println(t.toString());
		}
	}

	/**
	 * Suppression des temps qui n'ont pas de suivant
	 */
	public void supprsuivantvide() {
		Iterator<Temps> it = locale.iterator();
		Temps a;
		while (it.hasNext()) {
			a = it.next();
			if (a.getSuiv().isEmpty()) {
				it.remove();
			}
		}
	}
}