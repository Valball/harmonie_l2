package v2;

import java.util.*;

/**
 * Classe des Temps Un temps est un quadruplé de note sur un même instant t
 * C'est donc le soprano, l'alto, le tenor et la basse
 * 
 * @author Valentin
 * 
 */
public class Temps {

	private int sop;
	private int ten;
	private int alt;
	private int bas;
	private Accord acc;
	private int beaute;
	// la beaute du temps
	private int nombre;
	// le nombre de suivant du temps
	private ArrayList<Temps> suiv;

	/**
	 * Retourne le nombre de suivant du temps
	 */
	public int getNombre() {
		return nombre;
	}

	public void setNombre(int nombre) {
		this.nombre = nombre;
	}

	/**
	 * Constructeur de l'objet temps
	 * 
	 * @param sop
	 *            Le soprano du temps
	 * @param alt
	 *            L'alto du temps
	 * @param ten
	 *            Le tenor du temps
	 * @param bas
	 *            La basse du temps
	 * @param acc
	 *            L'accord du temps
	 */
	public Temps(int sop, int alt, int ten, int bas, Accord acc) {
		this.sop = sop;
		this.ten = ten;
		this.alt = alt;
		this.bas = bas;
		this.acc = acc;
		nombre = -1;
		// initialise le nombre de suivant
		beaute = 0;
		// initialise la beaute du temps
		suiv = new ArrayList<Temps>();
		// initialise la liste de suivant
	}

	/**
	 * Constructeur de l'objet temps
	 * 
	 * @param suiv
	 *            La liste des suivants du temps
	 */
	public Temps(ArrayList<Temps> suiv) {
		this.suiv = suiv;
		nombre = -1;
		bas = 0;
	}

	/**
	 * Retourne le soprano
	 */
	public int getSop() {
		return sop;
	}

	/**
	 * Retourne le tenor
	 */
	public int getTen() {
		return ten;
	}

	/**
	 * Retourne l'alto
	 */
	public int getAlt() {
		return alt;
	}

	/**
	 * Retourne la basse
	 */
	public int getBas() {
		return bas;
	}

	/**
	 * Retourne l'accord
	 */
	public Accord getAcc() {
		return acc;
	}

	/**
	 * Retourne la liste des Temps suivant du Temps
	 */
	public ArrayList<Temps> getSuiv() {
		return suiv;
	}

	/**
	 * Retourne vrai si la règle d'enchainement 1 est respecté
	 */
	public boolean regle1() {
		// Retourne vrai si le sop est compris entre 14 et 26, si l'alto est
		// compris entre 11 et 22, si le tenor est comprise entre 7 et 19 et si
		// la basse est comprise entre 3 et 14
		return ((sop >= 14) && (sop <= 26) && (alt >= 11) && (alt <= 22)
				&& (ten >= 7) && (ten <= 19) && (bas >= 3) && (bas <= 14));
	}

	/**
	 * Retourne vrai si la règle d'enchainement 2 est respecté
	 */
	public boolean regle2() {
		// Vrai si le soprano est supérieur à l'alto lui même supérieur au tenor
		// lui même supérieur à la basse
		return ((bas < ten) && (ten < alt) && (alt < sop));
	}

	/**
	 * Retrourne vrai si l’accord correspond à l’accord IVb.
	 */
	public boolean règle4() {
		return acc.getNum() == 8;
	}

	/**
	 * Retourne vrai si la règle 5 est respecté
	 * 
	 * @param a
	 *            Le Temps suivant à tester
	 */
	public boolean règle5(Temps a) {
		switch (acc.getNum()) {
		case 1:
			return a.getAcc().getNum() != 8;
		case 2:
			return a.getAcc().getNum() == 5 || a.getAcc().getNum() == 6;
		case 3:
			return a.getAcc().getNum() != 1 && a.getAcc().getNum() != 8;
		case 4:
			return a.getAcc().getNum() != 8;
		case 8:
			return a.getAcc().getNum() == 1;
		case 5:
			return a.getAcc().getNum() == 1 || a.getAcc().getNum() == 3
					|| a.getAcc().getNum() == 5 || a.getAcc().getNum() == 8;
		case 6:
			return a.getAcc().getNum() == 1 || a.getAcc().getNum() == 3
					|| a.getAcc().getNum() == 5 || a.getAcc().getNum() == 4;
		case 7:
			return a.getAcc().getNum() == 1 || a.getAcc().getNum() == 3;
		}
		return false;
	}

	/**
	 * Retourne vrai si la règle 6 est respecté
	 * 
	 * @param n2
	 *            Le Temps suivant à tester
	 */
	public boolean regle6(Temps n2) {
		int sop2 = n2.sop;
		int alt2 = n2.alt;
		int ten2 = n2.ten;
		int bas2 = n2.bas;
		// Recherche des accords
		Accord acc = this.getAcc();
		Accord acc2 = n2.getAcc();
		// la différence entre les deux notes ne peut pas excéder 6
		if ((Math.abs(sop - sop2) <= 6) && (Math.abs(alt - alt2) <= 6)
				&& (Math.abs(ten - ten2) <= 6) && (Math.abs(bas - bas2) <= 6)) {
			// Quand la différence entre les deux notes excèdent 2
			// (strictement),
			if ((Math.abs(sop - sop2) > 2)) {
				// elles doivent être de même nature (tonique, tierce ou quinte)
				// dans leurs accords respectifs
				if (acc.estDansAcc(sop) != acc2.estDansAcc(sop2)) {
					return false;
				}
				if ((Math.abs(alt - alt2) > 2)) {
					if (acc.estDansAcc(alt) != acc2.estDansAcc(alt2))
						return false;
				}
				if ((Math.abs(ten - ten2) > 2)) {
					if (acc.estDansAcc(ten) != acc.estDansAcc(ten2))
						return false;
				}
				if ((Math.abs(bas - bas2) > 2)) {
					if (acc.estDansAcc(bas) != acc.estDansAcc(bas2))
						return false;
				}
			}
			return this.regle62(n2);
		}
		return false;
	}

	/**
	 * Suite de la règle 6
	 * 
	 * @param n2
	 *            Le Temps suivant à tester
	 */
	public boolean regle62(Temps n2) {
		boolean bool = true;
		if (n2.getAcc().estDansAcc(sop) != 0) {
			// si le soprano actuel est dans l'accord suivant
			bool = n2.estdansnote(sop);
			// le soprano est-il dans la note suivante ?
		}
		if (!bool)
			return false;
		if (n2.getAcc().estDansAcc(alt) != 0) {
			bool = n2.estdansnote(alt);
			// idem avec l'alto ...
		}
		if (!bool)
			return false;
		if (n2.getAcc().estDansAcc(ten) != 0) {
			bool = n2.estdansnote(ten);
			// ... et le tenor
		}
		return bool;

	}

	/**
	 * Retourne la beauté de l'accord
	 */
	public int getBeaute() {
		return beaute;
	}

	/**
	 * Test si une note dans le temps actuel
	 * 
	 * @param n
	 *            la note à testé
	 */
	public boolean estdansnote(int n) {
		return n == sop || n == ten || n == alt || n == bas;
	}

	/**
	 * Rajoute un Temps à la liste des suivants
	 * 
	 * @param suiv
	 *            Le temps à ajouter
	 */
	public void add(Temps suiv) {
		this.suiv.add(suiv);
	}

	/**
	 * Test si la liste des suivants du Temps courants est vide
	 */
	public boolean isLast() {
		return suiv.isEmpty();
	}

	/**
	 * Calcule la beauté de chaque suivants du temps courant et les ordonne de
	 * manière croissante
	 * 
	 * @param beaute
	 *            Le numéro de la règle de beauté à appliquer
	 */
	public void calculetordonne(int beaute) {
		Iterator<Temps> it = suiv.iterator();
		Temps t;
		while (it.hasNext()) {
			t = it.next();
			switch (beaute) {
			case 1:
				t.beaute = Beauté.beaute1(t);
				break;
			case 2:
				t.beaute = Beauté.beaute2(this, t);
				break;
			case 3:
				t.beaute = Beauté.beaute3(this, t);
				break;
			case 4:
				t.beaute = Beauté.beaute4(this, t);
				break;
			} // calcule la beauté du suivant par rapport à la règle de beauté
				// indiqué
		}
		// classe qui ordonne les temps par rapport à leur beauté
		class Compare implements Comparator<Temps> {
			public int compare(Temps arg0, Temps arg1) {
				if (arg0.getBeaute() > arg1.getBeaute())
					return 1;
				else if (arg0.getBeaute() == arg1.getBeaute())
					return 0;
				else
					return -1;
			}
		}
		Collections.sort(suiv, new Compare());
	}

	/**
	 * Renvoie la String du Temps
	 */
	public String toString() {
		return "Temps [sop=" + sop + ", alt=" + alt + " , ten=" + ten
				+ ", bas=" + bas + "]";
	}
}
