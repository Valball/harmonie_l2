package v2;

/**
 * Un Accord tel défini dans le sujet
 * 
 */
public class Accord {
	/**
	 * Numéro de l'accord
	 */
	private int num;
	/**
	 * Tonique de l'accord
	 */
	private int ton;
	/**
	 * Tierce de l'accord
	 */
	private int tie;
	/**
	 * Quinte de l'accord
	 */
	private int qui;

	/**
	 * Constructeur d'un accord
	 * 
	 * @param n
	 *            Numéro de l'accord
	 */
	public Accord(int n) {
		assert (n < 9 && n > 0);
		num = n;
		switch (n) {
		case 1:
			ton = 0;
			tie = 2;
			qui = 4;
			return;
		case 2:
			ton = 1;
			tie = 3;
			qui = 5;
			return;
		case 3:
			ton = 2;
			tie = 4;
			qui = 6;
			return;
		case 4:
			ton = 3;
			tie = 5;
			qui = 0;
			return;
		case 5:
			ton = 4;
			tie = 6;
			qui = 1;
			return;
		case 6:
			ton = 5;
			tie = 0;
			qui = 2;
			return;
		case 7:
			ton = 6;
			tie = 1;
			qui = 3;
			return;
		case 8:
			ton = 3;
			tie = 5;
			qui = 0;
			return;
			// Accord IVb
		}
	}

	/**
	 * Retourne le numéro de l'accord
	 */
	public int getNum() {
		return num;
	}

	/**
	 * Retourne la Tonique de l'accord
	 */
	public int getTon() {
		return ton;
	}

	/**
	 * Retourne la Tierce de l'accord
	 */
	public int getTie() {
		return tie;
	}

	/**
	 * Retourne la Quinte de l'accord
	 */
	public int getQui() {
		return qui;
	}

	/**
	 * Test si une note donnée est tonique tierce ou quinte
	 * 
	 * @return Un entier signifiant soit tonique, soit tierce, soit quinte, soit
	 *         pas présent.
	 */
	public int estDansAcc(int n2) {
		int n = n2 % 7;
		if (n == ton) {
			return 1;
		} else {
			if (n == tie) {
				return 2;
			} else {
				if (n == qui) {
					return 3;
				} else {
					return 0;
				}
			}
		}
	}

}