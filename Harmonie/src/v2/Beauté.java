package v2;

/**
 * Classe qui contient les règles de beauté
 */
public class Beauté {
	/**
	 * Regle de beauté 1 qui retourne la difference entre l'alto et le tenor
	 */
	public static int beaute1(Temps t) {
		return t.getAlt() - t.getTen();
	}

	/**
	 * Regle de beauté 2 qui calcule les tenues de l'alto et du tenor
	 */
	public static int beaute2(Temps t1, Temps t2) {
		if (t2.getAlt() == t1.getAlt()) {
			if (t2.getTen() == t1.getTen()) {
				return t1.getBeaute() + 2;
				// si l'alto et le tenor sont identiques au temps précédent
				// alors on ajoute 2 à la beauté du temps précédent
			} else {
				return t1.getBeaute() + 1;
				// si seulement l'alto ou le tenor sont identiques au temps
				// précédent alors on ajoute 1 à la beauté du temps précédent
			}
		} else {
			if (t2.getTen() == t1.getTen()) {
				return t1.getBeaute() + 1;
				// si seulement l'alto ou le tenor sont identiques au temps
				// précédent alors on ajoute 1 à la beauté du temps précédent
			} else {
				return 1;
				// si l'alto et le tenor sont différents alors on réinitialise
				// la beauté à 1
			}
		}
	}

	/**
	 * Règle de beauté 3 qui retourne la difference de la basse entre deux temps
	 */
	public static int beaute3(Temps t1, Temps t2) {
		return Math.abs(t1.getBas() - t2.getBas());
	}

	/**
	 * Règle de beauté 4 qui est une combinaison des règlse de beauté 1, 2 et 3
	 */
	public static int beaute4(Temps t1, Temps t2) {
		return ((beaute3(t1, t2) + beaute2(t1, t2)) / beaute1(t2));
	}
}