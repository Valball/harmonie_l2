package test;

import static org.junit.Assert.*;

import org.junit.Test;
import v2.*;

/**
 * Test sur les accord
 * 
 */
public class AccordTest {

	@Test
	public void test() {
		Accord a1 = new Accord(1);
		Accord a4 = new Accord(4);
		Accord a6 = new Accord(6);
		if (a1.estDansAcc(21) != 1)
			fail("Retour tonique inccorect");
		if (a6.estDansAcc(21) != 2)
			fail("Retour tierce inccorect");
		if (a4.estDansAcc(21) != 3)
			fail("Retour quinte inccorect");
		if (a1.estDansAcc(1) != 0)
			fail("Retour inexistant inccorect");
	}

}
