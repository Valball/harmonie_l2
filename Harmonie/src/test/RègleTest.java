package test;

import static org.junit.Assert.*;
import org.junit.Test;
import v2.*;

/**
 * Test des méthode des règles d'enchainement
 * 
 * @see Temps
 */
public class RègleTest {

	@Test
	/**
	 * Test de la règle 6
	 */
	public void testrègle6() {
		Temps n1 = new Temps(21, 18, 16, 7, new Accord(1));
		Temps n2 = new Temps(21, 19, 17, 10, new Accord(4));
		Temps n3 = new Temps(14, 11, 9, 7, new Accord(1));
		Temps n4 = new Temps(21, 16, 11, 7, new Accord(1));
		if (!n1.regle6(n2)) {
			fail("règle 6 incorrect 1");
		}
		if (n1.regle6(n3)) { // Pour un chant donné et pour deux Temps succesifs
								// leur différence excèdent 6
			fail("règle 6 incorrect 2");
		}
		if (n1.regle6(n4)) { // Pour un chant donné et pour deux Temps succesifs
								// si leur différence est supérieur à 2 ils
								// doivent être de même nature
			fail("règle 6 incorrect 3");
		}
	}

	@Test
	/**
	 * Test de la règle 5
	 */
	public void testrègle5() {
		Temps n1 = new Temps(21, 18, 16, 7, new Accord(1));
		Temps n2 = new Temps(21, 19, 17, 10, new Accord(4));
		Temps n3 = new Temps(21, 19, 17, 10, new Accord(8));
		if (!n1.règle5(n2)) {
			fail("règle 5 incorrect 1");
		}
		if (n1.règle5(n3)) { // L'accord 1 est suivi de l'accord 8
			fail("règle 5 incorrect 2");
		}
	}

	@Test
	/**
	 * Test de la règle 1
	 */
	public void testregle1() {
		Temps n1 = new Temps(21, 18, 16, 7, new Accord(1));
		if (!n1.regle1())
			fail("Rejete un exemple normalement vrai");
		n1 = new Temps(27, 18, 16, 7, new Accord(1));
		Temps n2 = new Temps(24, 23, 16, 7, new Accord(1));
		Temps n3 = new Temps(22, 21, 20, 7, new Accord(1));
		Temps n4 = new Temps(21, 18, 17, 16, new Accord(1));
		if (n1.regle1() || n2.regle1() || n3.regle1() || n4.regle1())
			fail("Accepte des exemples normalement faux");
	}

	@Test
	/**
	 * Test de la règle 2
	 */
	public void testregle2() {
		Temps n1 = new Temps(21, 18, 16, 7, new Accord(1));
		if (!n1.regle2())
			fail("Rejete un exemple normalement vrai");
		n1 = new Temps(21, 22, 16, 7, new Accord(1));
		if (n1.regle2())
			fail("Accepte un exemple normalement faux");
	}

	@Test
	/**
	 * Test de la règle 4
	 */
	public void testregle4() {
		Temps n1 = new Temps(21, 18, 16, 7, new Accord(1));
		if (n1.règle4())
			fail("Accepte un exemple normalement faux");
		n1 = new Temps(21, 18, 16, 7, new Accord(8));
		if (!n1.règle4())
			fail("Rejete un exemple normalement vrai");
	}
}