package exceptions;

/**
 * Exception dans le format d'un des paramètre
 * 
 */
@SuppressWarnings("serial")
public class ParamFormatException extends Exception {
	private int numparam;

	/**
	 * Cronstructeur de l'exption
	 * 
	 * @param n
	 *            Le numéro du paramètre qui lève l'exception
	 */
	public ParamFormatException(int n) {
		numparam = n;
	}

	public int getNumparam() {
		return numparam;
	}

}
