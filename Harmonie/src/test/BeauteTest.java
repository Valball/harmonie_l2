package test;

import static org.junit.Assert.*;

import org.junit.Test;
import v2.Temps;
import v2.Beauté;
import v2.Accord;

/**
 * Test sur les règle de beauté
 * 
 * @see Beauté
 * 
 */
public class BeauteTest {

	@Test
	/**
	 * Test de la beauté 1
	 */
	public void test1() {
		Temps t = new Temps(21, 18, 16, 7, new Accord(1));
		if (Beauté.beaute1(t) != 2)
			fail("Beauté 1 incorrect");
	}

	@Test
	/**
	 * Test de la beauté 2
	 */
	public void test2() {
		Temps t1 = new Temps(21, 18, 16, 7, new Accord(1));
		Temps t2 = new Temps(21, 18, 16, 7, new Accord(1));
		if (Beauté.beaute2(t1, t2) != 2)
			fail("Beauté 2 inccorect (+2)");
		t2 = new Temps(21, 18, 15, 7, new Accord(1));
		if (Beauté.beaute2(t1, t2) != 1)
			fail("Beauté 2 inccorect (+1)");
		t2 = new Temps(21, 17, 16, 7, new Accord(1));
		if (Beauté.beaute2(t1, t2) != 1)
			fail("Beauté 2 inccorect (+1)(2)");
		t2 = new Temps(21, 17, 15, 9, new Accord(1));
		if (Beauté.beaute2(t1, t2) != 1)
			fail("Beauté 2 inccorect (=1)");
	}

	@Test
	/**
	 * Test de la beauté 3
	 */
	public void test3() {
		Temps t1 = new Temps(21, 18, 16, 7, new Accord(1));
		Temps t2 = new Temps(21, 18, 16, 14, new Accord(1));
		if (Beauté.beaute3(t1, t2) != 7)
			fail("Beauté 3 inccorect");
	}

	@Test
	/**
	 * Test de la beauté 4
	 */
	public void test4() {
		Temps t1 = new Temps(21, 18, 16, 7, new Accord(1));
		Temps t2 = new Temps(21, 18, 16, 14, new Accord(1));
		if (Beauté.beaute4(t1, t2) != 4)
			fail("Beauté 4 inccorect");
	}

}