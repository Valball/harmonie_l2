package test;

import static org.junit.Assert.*;

import org.junit.Test;

import v2.*;
import exceptions.EncodeException;

/**
 * Classe de test de certaine méthode du Main
 * 
 * @see Main
 * 
 */
public class MainTest {
	@Test
	public void nombre() {
		Temps n1 = new Temps(21, 18, 16, 7, new Accord(1));
		Temps n2 = new Temps(21, 19, 17, 10, new Accord(4));
		Temps n3 = new Temps(21, 19, 16, 12, new Accord(6));
		Temps n4 = new Temps(22, 20, 18, 13, new Accord(5));
		Temps n5 = new Temps(23, 21, 18, 7, new Accord(1));
		Temps n6 = new Temps(23, 20, 18, 9, new Accord(3));
		Temps n7 = new Temps(22, 19, 17, 8, new Accord(2));
		Temps n8 = new Temps(22, 20, 18, 13, new Accord(5));
		Temps n9 = new Temps(21, 19, 17, 10, new Accord(8));
		Temps n10 = new Temps(23, 22, 18, 7, new Accord(1));
		Temps n11 = new Temps(22, 20, 18, 11, new Accord(5));
		Temps n12 = new Temps(22, 20, 18, 11, new Accord(5));
		Temps n13 = new Temps(21, 19, 17, 10, new Accord(8));
		n1.add(n2);
		n1.add(n3);
		n1.add(n4);
		n1.add(n5);
		n2.add(n9);
		n2.add(n6);
		n3.add(n6);
		n3.add(n7);
		n3.add(n8);
		n4.add(n9);
		n4.add(n7);
		n4.add(n8);
		n5.add(n9);
		n6.add(n10);
		n7.add(n10);
		n7.add(n11);
		n7.add(n12);
		n8.add(n11);
		n8.add(n12);
		n9.add(n11);
		n9.add(n12);
		n9.add(n13);
		int no = Main.nombre(n1);
		if (no != 21)
			fail("nombre incorrect");
	}

	@Test
	public void decodage() throws EncodeException {
		if (Main.encode("-:1") != 113)
			fail("encodage incorrect");
		if (Main.decodesop(Main.encode("-:1")) != 28)
			fail("decodage soprano incorrect");
		if (Main.decodetemps(Main.encode("-:1")) != 1)
			fail("decodage temps incorrect");
	}
}